import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {TransactionService} from './transactions/services/transaction.service';
import {HttpClientModule} from '@angular/common/http';
import {TransactionResolver} from './Models/transaction.resolver';


@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [HttpClientModule, BrowserModule, IonicModule.forRoot(), AppRoutingModule],
    providers: [TransactionService, TransactionResolver, {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}],
    bootstrap: [AppComponent],
})
export class AppModule {
}
