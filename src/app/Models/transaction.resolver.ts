import {Injectable} from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute} from '@angular/router';
import {Observable, of} from 'rxjs';
import {TransactionService} from '../transactions/services/transaction.service';
import {Transaction} from './transaction';

@Injectable()
export class TransactionResolver implements Resolve<any> {


    constructor(private router: Router, private transactionService: TransactionService) {
    }

    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): any {

        return this.transactionService.getById(route.queryParams.id);

    }
}
