export interface Recap {
    total: number;
    debit: number;
    credit: number;
}
