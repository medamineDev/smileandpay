export interface Transaction {
    id: string |number;
    datetime: Date | string;
    amount: number | string;
    type: string;
    mode: string;
    commentaire: string;
}
