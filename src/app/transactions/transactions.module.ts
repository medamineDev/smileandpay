import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {TransactionsPageRoutingModule} from './transactions-routing.module';

import {TransactionsPage} from './transactions.page';

import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {TransactionService} from './services/transaction.service';
import {HttpClientModule} from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';
import {TransactionResolver} from '../Models/transaction.resolver';

@NgModule({
    imports: [
        NgxDatatableModule,
        CommonModule,
        FormsModule,
        IonicModule,
        HttpClientModule,
        NgxPaginationModule,
        TransactionsPageRoutingModule
    ],

    declarations: [TransactionsPage],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TransactionsPageModule {
}
