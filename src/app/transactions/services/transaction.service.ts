import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {Transaction} from '../../Models/transaction';
import {Recap} from '../../Models/recap';

@Injectable({
    providedIn: 'root'
})
export class TransactionService {

    private sorted = false;
    public DeletedTransactions = [];
    private ALL_TRANSACTIONS_LABEL = 'all_transactions';
    private CREDIT_LABEL = 'crédit';
    private DEBIT_LABEL = 'débit';

    constructor(private httpClient: HttpClient) {
    }

    /**
     * Get Transaction list from remote server
     * Returns a Observable<[Transaction]>
     */
    list(): Observable<[Transaction]> | Promise<any> | any {
        return this.httpClient.get('./assets/db/transactions.json')
            .pipe(
                map((data: { transactions: [any] }) => data.transactions),
                map((data: [Transaction]) => {
                    data.map((value: Transaction) => {
                        value.datetime = value.datetime.toString().slice(0, 10);
                        value.id = Number(value.id);
                        value.amount = Number(value.amount);
                    });
                    return data;
                })
            );
    }

    /**
     * Get Transaction Details By Id
     * Returns a Observable<[Transaction]>
     * @param idTrans : string
     */
    async getById(idTrans: string): Promise<Observable<Transaction>> {
        const transaction = await this.list().toPromise().then(data => {
            data = data.filter((trans: Transaction) => Number(trans.id) === Number(idTrans));
            return data;
        });

        return new Promise((resolve, reject) => {
            resolve(of(transaction));
        });
    }


    /**
     * Get Sum by Property
     * Returns number
     * @param data : Transaction
     * @param property : string
     */
    getSum(data, property) {
        return data.reduce((sum, record) => {
            if (property === this.ALL_TRANSACTIONS_LABEL) {
                return sum + Number(record.amount);
            } else {
                if (record.type === property) {
                    return sum + Number(record.amount);
                } else {
                    return sum;
                }
            }
        }, 0);
    }

    /**
     * Get Recap with handling removed transactions
     * Returns Promise<Recap>
     */
    getSumByProperty(): Promise<Recap> {
        return this.list().toPromise().then(data => {
            data = data.filter((trans: Transaction) => !this.DeletedTransactions.includes(Number(trans.id)));

            const totalAmount = this.getSum(data, this.ALL_TRANSACTIONS_LABEL);
            const debAmount = this.getSum(data, this.DEBIT_LABEL);
            const creditAmount = this.getSum(data, this.CREDIT_LABEL);

            return {total: totalAmount, credit: creditAmount, debit: debAmount};
        });
    }


    /**
     * Get Sorted list by property
     * Returns an Observable<[Transaction]>
     * @param transactions : Array[Transaction]
     * @param property : string
     */
    getSortedRows(transactions, property): Observable<[Transaction]> {

        if (this.sorted) {
            transactions.sort((a, b) => {
                if (a[property] > b[property]) {
                    return 1;
                }
                if (a[property] < b[property]) {
                    return -1;
                }
                return 0;
            });

        } else {
            transactions.sort((a, b) => {
                if (a[property] < b[property]) {
                    return 1;
                }
                if (a[property] > b[property]) {
                    return -1;
                }
                return 0;
            });
        }
        this.sorted = !this.sorted;
        return of(transactions);
    }
}
