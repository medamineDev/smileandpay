import {Component, OnInit} from '@angular/core';
import {Transaction} from '../Models/transaction';
import {TransactionService} from './services/transaction.service';


@Component({
    selector: 'app-transactions',
    templateUrl: './transactions.page.html',
    styleUrls: ['./transactions.page.scss'],
})
export class TransactionsPage implements OnInit {

    public $transactionList: any;
    public transactionListArray = [];
    public DeletedTransactions = [];
    public p;

    constructor(private  transactionService: TransactionService) {

    }

    ionViewWillEnter() {
        this.getNewTransactionData();
    }

    async orderBy(property) {
        this.$transactionList = this.transactionService.getSortedRows(await this.$transactionList.toPromise(), property);
        this.getNewTransactionData();
    }

    getNewTransactionData() {
        this.$transactionList.subscribe(data => {
            this.transactionListArray = data.filter((trans: Transaction) => !this.DeletedTransactions.includes(Number(trans.id)));
        });
    }

    ngOnInit() {
        this.$transactionList = this.transactionService.list();
        this.DeletedTransactions = this.transactionService.DeletedTransactions;
        this.getNewTransactionData();
    }

}
