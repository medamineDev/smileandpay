import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';
import {Transaction} from '../Models/transaction';
import {Observable} from 'rxjs';
import {TransactionService} from '../transactions/services/transaction.service';

@Component({
    selector: 'app-transaction-details',
    templateUrl: './transaction-details.page.html',
    styleUrls: ['./transaction-details.page.scss'],
})
export class TransactionDetailsPage implements OnInit {
    public transaction: Transaction;
    private transactionObs: Observable<Transaction>;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private transactionService: TransactionService) {

    }

    DeleteTransaction(id) {
        this.transactionService.DeletedTransactions.push(id);
        this.router.navigate(['transactions']);
    }

    ngOnInit() {
        this.transactionObs = this.activatedRoute.snapshot.data['transaction'];

        this.transactionObs.subscribe(data => {
            this.transaction = data[0];
        });
    }
}
