import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent {
    public appPages = [
        {title: 'Liste de Mes Transactions', url: '/transactions', icon: 'paper-plane'},
        {title: 'Récapitulatif', url: '/recap', icon: 'archive'},
    ];

    constructor() {
    }
}
