import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {TransactionResolver} from './Models/transaction.resolver';

const routes: Routes = [
    {
        path: 'transactions',
        loadChildren: () => import('./transactions/transactions.module').then(m => m.TransactionsPageModule)
    },
    {
        path: 'recap',
        loadChildren: () => import('./recap/recap.module').then(m => m.RecapPageModule)
    },
    {
        path: 'transaction-details',
        resolve: {transaction: TransactionResolver},
        loadChildren: () => import('./transaction-details/transaction-details.module').then(m => m.TransactionDetailsPageModule)
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    providers: [TransactionResolver],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
