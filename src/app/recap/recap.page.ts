import {Component, OnInit} from '@angular/core';
import {TransactionService} from '../transactions/services/transaction.service';
import {Recap} from '../Models/recap';

@Component({
    selector: 'app-recap',
    templateUrl: './recap.page.html',
    styleUrls: ['./recap.page.scss'],
})
export class RecapPage implements OnInit {

    public recap: Recap;

    constructor(private transactionService: TransactionService) {
    }

    ngOnInit() {
        this.transactionService.getSumByProperty().then(recap => {
            this.recap = recap;
        });
    }

}
