(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["recap-recap-module"],{

/***/ "6NFC":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/recap/recap.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-menu-button autoHide=\"false\"></ion-menu-button>\n<ion-header>\n  <ion-toolbar>\n    <ion-title>Récapitulatif</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "CBpL":
/*!***************************************!*\
  !*** ./src/app/recap/recap.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZWNhcC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "Imkx":
/*!*************************************!*\
  !*** ./src/app/recap/recap.page.ts ***!
  \*************************************/
/*! exports provided: RecapPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecapPage", function() { return RecapPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_recap_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./recap.page.html */ "6NFC");
/* harmony import */ var _recap_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./recap.page.scss */ "CBpL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let RecapPage = class RecapPage {
    constructor() { }
    ngOnInit() {
    }
};
RecapPage.ctorParameters = () => [];
RecapPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-recap',
        template: _raw_loader_recap_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_recap_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], RecapPage);



/***/ }),

/***/ "dRo5":
/*!***********************************************!*\
  !*** ./src/app/recap/recap-routing.module.ts ***!
  \***********************************************/
/*! exports provided: RecapPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecapPageRoutingModule", function() { return RecapPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _recap_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./recap.page */ "Imkx");




const routes = [
    {
        path: '',
        component: _recap_page__WEBPACK_IMPORTED_MODULE_3__["RecapPage"]
    }
];
let RecapPageRoutingModule = class RecapPageRoutingModule {
};
RecapPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], RecapPageRoutingModule);



/***/ }),

/***/ "lS4A":
/*!***************************************!*\
  !*** ./src/app/recap/recap.module.ts ***!
  \***************************************/
/*! exports provided: RecapPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecapPageModule", function() { return RecapPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _recap_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./recap-routing.module */ "dRo5");
/* harmony import */ var _recap_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./recap.page */ "Imkx");







let RecapPageModule = class RecapPageModule {
};
RecapPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _recap_routing_module__WEBPACK_IMPORTED_MODULE_5__["RecapPageRoutingModule"]
        ],
        declarations: [_recap_page__WEBPACK_IMPORTED_MODULE_6__["RecapPage"]]
    })
], RecapPageModule);



/***/ })

}]);
//# sourceMappingURL=recap-recap-module.js.map